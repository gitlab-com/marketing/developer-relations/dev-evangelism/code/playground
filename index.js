const express = require('express');
const bodyParser = require('body-parser');
const app = express();

app.use(bodyParser.json());

let users = [
  {id: 1, name: 'John Doe'},
  {id: 2, name: 'Jane Doe'},
];

app.get('/users', (req, res) => {
  res.send(users);
});

app.get('/users/:id', (req, res) => {
  const user = users.find(u => u.id === req.params.id);
  res.send(user);
});

app.post('/users', (req, res) => {
  const user = {id: users.length + 1, name: req.body.name};
  users.push(user);
  res.send(user);
});

app.put('/users/:id', (req, res) => {
  const user = users.find(u => u.id === req.params.id);
  user.name = req.body.name;
  res.send(user);
});

app.delete('/users/:id', (req, res) => {
  users = users.filter(u => u.id !== req.params.id);
  res.send(users);
});

app.listen(3000, () => console.log('Listening on port 3000...'));
